import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/dashboard/dashboard-pasien.dart';
import 'package:jdt13/order/QRCodeImageTreatment.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../api/api-service.dart';

class HistoryPasien extends StatefulWidget {
  @override
  _HistoryPasienState createState() => new _HistoryPasienState();
}

class _HistoryPasienState extends State<HistoryPasien> {
  late SharedPreferences prefs;
  late List data = [];

  Future<List<dynamic>> makeRequest() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/treatment/pasien/' + prefs.getInt("userId").toString();
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      var extractData = json.decode(response.body);
      data = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return data;
  }

  String formatDate(String date) {
    initializeDateFormatting('id_ID');
    final parsedDate = DateFormat('yyyy-MM-dd').parse(date);
    final formattedDate = DateFormat.yMMMMd('id_ID').format(parsedDate);
    return formattedDate;
  }

  @override
  void initState() {
    this.makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new DashboardPasien(),
                  ));
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: Colors.blue,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                alignment: Alignment.topLeft,
                child: Text(
                  "Riwayat Konsultasi",
                  style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, i) {
                return Card(
                  child: Column(children: [
                    ListTile(
                      title: Text(
                        data[i]["spesialisasi"].toString(),
                        style: TextStyle(color: Colors.black),
                      ),
                      subtitle: Text(
                        'Nama Dokter: ' + data[i]["namaDokter"].toString() + 
                        '\nTanggal Konsultasi: ' + formatDate(data[i]["tanggalKonsul"]),
                        style: TextStyle(color: Colors.blue[450]),
                      ),
                      leading: Image.network(
                        "https://img.freepik.com/premium-vector/hospital-physician-counseling-patient-silhouette-icon-consultation-patient-doctor_954341-28.jpg",
                        fit: BoxFit.cover,
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.topCenter,
                      ),
                      trailing: Text(
                        data[i]["status"].toString(),
                        style: TextStyle(color: Colors.blue[450]),
                      ),
                    ),
                    Visibility(
                      visible: data[i]["status"].toString() == "Complete",
                      child: 
                      ElevatedButton(
                        onPressed: () {
                          prefs.setInt("treatmentId", data[i]["id"]);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => QRCodeImageTreatment()),
                          );
                        },
                        child: Text('QR Code'),
                      ),
                    ),
                    SizedBox(height: 10)
                  ],)
                );
              }
            ),
          ),
        ],
      ),
    ));
  }
}
