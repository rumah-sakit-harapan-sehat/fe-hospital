import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/order/medical-record.dart';
import 'package:jdt13/order/spesialisasi.dart';
import 'package:jdt13/order/transaction.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api/api-service.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class Spesialis {
  final int id;
  final String namaSpesialis;

  Spesialis({required this.id, required this.namaSpesialis});

  factory Spesialis.fromJson(Map<String, dynamic> json) {
    return Spesialis(
      id: json['id'],
      namaSpesialis: json['namaSpesialis'],
    );
  }
}

class _HomeState extends State<Home> {
  TextEditingController penumpangController = new TextEditingController();

  List<Spesialis> listSpesialis = [];
  Spesialis? selectedSpesialis;
  late List data = [];

  late String nama = "";
  String jenisKendaraan = 'Pesawat';
  String namaPetugas = "Viryal";
  String namaPenumpang = "Nada";
  int jenisKend = 1;
  int petugasNama = 1;
  var hrg = 50000;
  int bngku = 1;
  List<String> listPetugas=['Viryal', 'Ajeng', 'Siapa'];
  List<int> listIdPetugas = [1, 2, 3];
  List<String> listKendaraan=['Pesawat', 'Kereta', 'Bus'];

  void _addOrder(var menuId) async {
    final now = DateTime.now();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var response = await http.post(
      Uri.parse(ApiService.urlApi + '/tiket'),
      headers: <String, String>{
        'token': prefs.getString("token").toString(),
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'noBangku': bngku.toString(),
        'harga': hrg.toString(),
        'kendaraanId': bngku.toString(),
        'petugasId': petugasNama.toString(),
        'penumpangId': prefs.getInt("penumpangId").toString()
      }),
    );
    setState(() {
      var data = json.decode(response.body);
      final tiketId = data['tiketId'];
     
      if (response.statusCode == 201) {
        Fluttertoast.showToast(
           
            msg: "Add Menu Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: "Invalid Create Transaction",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }
  

  Future<List<dynamic>> makeRequest() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/menu';
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      var extractdata = json.decode(response.body);
      data = extractdata;
    });
      return data;
  }

  Future<void> fetchData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String role = prefs.getString("role").toString();

    if(role == "PASIEN") {
      final response = await http.get(Uri.parse(ApiService.urlApi + '/pasien/'+prefs.getInt("userId").toString()), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Bearer $bearerToken',
      });

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = jsonDecode(response.body);
        setState(() {
          nama = data['data']['namaPasien'];
        });
      } else {
        print('Failed to load data: ${response.statusCode}');
      }
    } else if(role == "DOKTER") {
      final response = await http.get(Uri.parse(ApiService.urlApi + '/dokter/'+prefs.getInt("userId").toString()), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Bearer $bearerToken',
      });

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = jsonDecode(response.body);
        setState(() {
          nama = data['data']['namaDokter'];
        });
      } else {
        print('Failed to load data: ${response.statusCode}');
      }
    }

    
  }

  Future<void> fetchDataSpesialis() async {
    final response = await http.get(Uri.parse(ApiService.urlApi + '/spesialis/'));

    if (response.statusCode == 200) {
      final List<dynamic> data = jsonDecode(response.body);

      setState(() {
        listSpesialis = data.map((item) => Spesialis.fromJson(item)).toList();
      });
    } else {
      throw Exception('Failed to load spesialis data');
    }
  }


  Future<void> fetchDataOnInitialize() async {
    await makeRequest();
    await fetchData();
    fetchDataSpesialis();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: 30.0, left: 5.0, right: 5.0, bottom: 40.0
            ),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    child: ClipOval(
                      child: SizedBox.fromSize(
                        size: Size.fromRadius(30),
                        child: Image.network(
                          'https://freesvg.org/img/abstract-user-flat-4.png',
                          fit: BoxFit.cover
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(90, 10, 0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Welcome, Rumah Sakit Harapan Sehat",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 20.0
                          ),
                        ),
                        Text(
                          nama,
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    )
                  ),
                ),
              ],
            )
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                ),
              ),
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 20.0),
                    child: Text(
                      "Pilih Menu",
                      style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
                    child: Card(
                      elevation: 30,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      color: Colors.white,
                      child: Container(
                        height: 330,
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3, // number of items in each row
                            mainAxisSpacing: 8.0, // spacing between rows
                            crossAxisSpacing: 3.0, // spacing between columns
                          ),
                          padding: EdgeInsets.all(8.0),
                          // padding around the grid
                          itemCount: data.length,
                          // total number of items
                          itemBuilder: (context, index) {
                            return Container(
                              color: Colors.white,
                              child: new GestureDetector(
                                onTap: () => _onClicked(
                                  data[index]["id"],
                                  data[index]["namaMenu"]
                                ),
                                child: Center(
                                  child: Column(
                                    children: [
                                      AspectRatio(
                                        aspectRatio: 18.0 / 11.0,
                                        child: CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            child: Image.network(
                                              data[index]['urlImage'],
                                              fit: BoxFit.cover,
                                              height: 40.0,
                                              width: 40.0,
                                              alignment: Alignment.topCenter,
                                              )),
                                      ),
                                      Container(
                                        color: Colors.white,
                                        child: Center(
                                          child: Text(
                                            data[index]['namaMenu'],
                                            style: TextStyle(
                                                fontSize: 10.0,
                                                color: Colors.black),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _onClicked(var menuId, var namaMenu) {
    if (menuId == 1) {
      Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new Spesialiasi(),
      ));
    } else if (menuId == 2) {
      Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new MedicalRecord(),
      ));
    } else if (menuId == 3) {
      Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new Transaction(),
      ));
    }
  }
}