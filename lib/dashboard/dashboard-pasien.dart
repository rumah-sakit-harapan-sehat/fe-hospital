import 'package:flutter/material.dart';
import 'package:jdt13/dashboard/history-pasien.dart';
import 'profile-pasien.dart';
import 'home.dart';

class DashboardPasien extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _DashboardPasienState();
  }
}

class _DashboardPasienState extends State<DashboardPasien> {
  int _currentIndex = 0;
  final List<Widget> _children = [Home(), HistoryPasien(), ProfilePasien()];

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.w500);
  final List<Widget> labels = [
    Text(
      "Home",
      style: optionStyle,
    ),
    Text(
      "History",
      style: optionStyle,
    ),
    Text(
      "Profile",
      style: optionStyle,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.redAccent,),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history, color: Colors.redAccent,),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person, color: Colors.redAccent,),
            label: ''
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

