import 'package:flutter/material.dart';
import 'package:jdt13/dashboard/history-resepsionis.dart';
import 'package:jdt13/dashboard/profile-resepsionis.dart';

class DashboardResepsionis extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _DashboardResepsionisState();
  }
}

class _DashboardResepsionisState extends State<DashboardResepsionis> {
  int _currentIndex = 0;
  final List<Widget> _children = [HistoryResepsionis(), ProfileResepsionis()];

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.w500);
  final List<Widget> labels = [
    Text(
      "Transaction",
      style: optionStyle,
    ),
    Text(
      "Profile",
      style: optionStyle,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.redAccent,),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person, color: Colors.redAccent,),
            label: ''
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

