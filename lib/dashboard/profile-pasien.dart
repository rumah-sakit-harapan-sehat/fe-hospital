import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/login/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api/api-service.dart';

class ProfilePasien extends StatefulWidget {
  @override
  _ProfilePasienState createState() => new _ProfilePasienState();
}

class _ProfilePasienState extends State<ProfilePasien> {

  late String nama = "";
  late String nik = "";

  Future<void> fetchData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String role = prefs.getString("role").toString();

    final response = await http.get(Uri.parse(ApiService.urlApi + '/pasien/'+prefs.getInt("userId").toString()), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    if (response.statusCode == 200) {
      final Map<String, dynamic> data = jsonDecode(response.body);
      setState(() {
        nama = data['data']['namaPasien'];
        nik = data['data']['nik'];
      });
    } else {
      print('Failed to load data: ${response.statusCode}');
    }    
  }

  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => Login()),
    );
  }

  Future<void> fetchDataOnInitialize() async {
    await fetchData();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My Profile'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/images/profile_patient.png'),
              ),
              SizedBox(height: 20),
              Text(
                nama,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text(
                'NIK - ' + nik,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey,
                ),
              ),
              SizedBox(height: 20),
              Row(
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  logout();
                },
                child: Text('Logout'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStatColumn(String label, String value) {
    return Column(
      children: <Widget>[
        Text(
          value,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 5),
        Text(
          label,
          style: TextStyle(
            fontSize: 16,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}