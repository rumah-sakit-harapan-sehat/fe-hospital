import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/order/QRCodeImageTreatment.dart';
import 'package:jdt13/order/create-medical-record.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../api/api-service.dart';

class HistoryDokter extends StatefulWidget {
  @override
  _HistoryDokterState createState() => new _HistoryDokterState();
}

class _HistoryDokterState extends State<HistoryDokter> {
  late SharedPreferences prefs;
  late List data = [];

  Future<List<dynamic>> makeRequest() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/treatment/dokter/' + prefs.getInt("userId").toString();
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      var extractData = json.decode(response.body);
      data = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return data;
  }

  String formatDate(String date) {
    initializeDateFormatting('id_ID');
    final parsedDate = DateFormat('yyyy-MM-dd').parse(date);
    final formattedDate = DateFormat.yMMMMd('id_ID').format(parsedDate);
    return formattedDate;
  }

  @override
  void initState() {
    this.makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                alignment: Alignment.topLeft,
                child: Text(
                  "Daftar Konsultasi",
                  style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, i) {
                return Card(
                  child: Column(children: [
                    ListTile(
                    title: Text(
                      data[i]["status"].toString(),
                      style: TextStyle(color: Colors.black, fontFamily: 'Nunito Sans'),
                    ),
                    subtitle: Text(
                      'Nama Pasien: ' + data[i]["namaPasien"].toString() + 
                      '\nTanggal Konsultasi: ' + formatDate(data[i]["tanggalKonsul"]),
                      style: TextStyle(color: Colors.blue[450], fontFamily: 'Nunito Sans'),
                    ),
                    leading: Image.network(
                      "https://img.freepik.com/premium-vector/hospital-physician-counseling-patient-silhouette-icon-consultation-patient-doctor_954341-28.jpg",
                      fit: BoxFit.cover,
                      height: 40.0,
                      width: 40.0,
                      alignment: Alignment.topCenter,
                    ),
                    trailing: IconButton(
                      icon: Icon(Icons.arrow_forward_sharp),
                      onPressed: () {
                        prefs.setInt("treatmentId", data[i]["id"]);
                        prefs.setInt("pasienId", data[i]["pasienId"]);
                        prefs.setString("tanggalKonsul", data[i]["tanggalKonsul"]);
                        if (data[i]["status"].toString() == "Incoming") {
                          Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new CreateMedicalRecord(),
                        ));
                        } else {
                          Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context) =>
                              new QRCodeImageTreatment(),
                          ));
                        }
                      },
                    ),
                  ),
                  ],)
                );
              }
            ),
          ),
        ],
      ),
    ));
  }
}
