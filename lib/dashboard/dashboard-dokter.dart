import 'package:flutter/material.dart';
import 'package:jdt13/dashboard/history-dokter.dart';
import 'package:jdt13/dashboard/profile-dokter.dart';

class DashboardDokter extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _DashboardDokterState();
  }
}

class _DashboardDokterState extends State<DashboardDokter> {
  int _currentIndex = 0;
  final List<Widget> _children = [HistoryDokter(), ProfileDokter()];

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.w500);
  final List<Widget> labels = [
    Text(
      "History",
      style: optionStyle,
    ),
    Text(
      "Profile",
      style: optionStyle,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: Colors.redAccent,),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person, color: Colors.redAccent,),
            label: ''
          )
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

