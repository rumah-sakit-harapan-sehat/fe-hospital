import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/dashboard/dashboard-dokter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class UpdateTreatment extends StatefulWidget {
  @override
  _UpdateTreatmentState createState() => new _UpdateTreatmentState();
}

class ObatContainer extends StatefulWidget {
  final Map<String, dynamic> obat;
  final Function() onDelete;
  final Function(int) onEditQuantity;
  final String namaObat;

  ObatContainer({
    required this.obat,
    required this.onDelete,
    required this.onEditQuantity,
    required this.namaObat,
  });

  @override
  _ObatContainerState createState() => _ObatContainerState();
}

class _ObatContainerState extends State<ObatContainer> {
  TextEditingController kuantitasController = TextEditingController();

  @override
  void initState() {
    super.initState();
    kuantitasController.text = widget.obat["kuantitas"].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
        border: Border.all(),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Nama Obat: ' + widget.namaObat),
          Row(
            children: [
              Text('Kuantitas: '),
              SizedBox(
                width: 50,
                child: TextField(
                  controller: kuantitasController,
                  keyboardType: TextInputType.number,
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ElevatedButton(
                onPressed: () {
                  widget.onEditQuantity(int.tryParse(kuantitasController.text) ?? 0);
                },
                child: Text('Update'),
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  widget.onDelete();
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _UpdateTreatmentState extends State<UpdateTreatment> {
  late SharedPreferences prefs;
  late List dataLayanan = [];
  late List dataKamar = [];
  late List dataObat = [];
  List<Map<String, dynamic>> layananList = [];
  int? selectedLayananId;
  List<Map<String, dynamic>> kamarList = [];
  int? selectedKamarId;
  List<Map<String, dynamic>> obatList = [];
  int? selectedObatId;
  List<Map<String, dynamic>> selectedObatList = [];

  TextEditingController diagnosisController = new TextEditingController();
  TextEditingController pesanController = new TextEditingController();
  TextEditingController layananIdController = new TextEditingController();
  TextEditingController kamarIdController = new TextEditingController();
  TextEditingController obatIdController = new TextEditingController();
  TextEditingController kuantitasObatController = new TextEditingController();

  String getNamaObat(int? obatId) {
    Map<String, dynamic>? selectedObat = obatList.firstWhere(
      (Map<String, dynamic> obat) => obat['id'] == obatId,
      orElse: () => Map<String, dynamic>.from({}),
    );

    return selectedObat != null ? selectedObat['namaObat'] ?? 'Nama Obat Not Found' : 'Obat not found';
  }

  Future<dynamic> fetchLayananData() async {
    prefs = await SharedPreferences.getInstance();
    String url = ApiService.urlApi + '/layanan';
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    setState(() {
      var extractData = json.decode(response.body);
      layananList = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return dataLayanan;
  }

  Future<dynamic> fetchKamarData() async {
    prefs = await SharedPreferences.getInstance();
    String url = ApiService.urlApi + '/kamar';
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    setState(() {
      var extractData = json.decode(response.body);
      kamarList = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return dataKamar;
  }

  Future<dynamic> fetchObatData() async {
    prefs = await SharedPreferences.getInstance();
    String url = ApiService.urlApi + '/obat';
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    setState(() {
      var extractData = json.decode(response.body);
      obatList = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return dataObat;
  }

  Future<void> fetchDataOnInitialize() async {
    await fetchLayananData();
    await fetchKamarData();
    fetchObatData();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  void updateTreatment() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();

    List<Map<String, String>> requestObat = selectedObatList.map((obat) {
      return {
        "obatId": obat["obatId"].toString(),
        "kuantitas": obat["kuantitas"].toString(),
      };
    }).toList();

    var requestBody = {
      'pasienId': prefs.getInt("pasienId").toString(),
      'dokterId': prefs.getInt("userId").toString(),
      'tanggalKonsul': prefs.getString("tanggalKonsul").toString(),
      'obat': requestObat,
      'layananId': layananIdController.text,
      'diagnosis': diagnosisController.text,
      'pesan': pesanController.text,
      'kamarId': kamarIdController.text,
    };

    final String requestBodyJson = jsonEncode(requestBody);

    var response = await http.put(Uri.parse(ApiService.urlApi +'/treatment/' + prefs.getInt("treatmentId").toString()),
      headers: <String, String>{
        'token': prefs.getString("token").toString(),
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $bearerToken',
      },
      body: requestBodyJson,
    );
    setState(() {
      var data = json.decode(response.body);

      if(response.statusCode == 200){
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new DashboardDokter(),
        ));
      } else {
        Fluttertoast.showToast(
            msg: "Save Data Treatment Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.blue,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                    alignment: Alignment.topLeft,
                    child: Text("Treatment", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 22.0),),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Pilih Layanan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: DropdownButtonFormField<int>(
                        decoration: InputDecoration(
                          hintText: 'Pilih Layanan',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),
                        value: selectedLayananId,
                        onChanged: (int? value) {
                          setState(() {
                            selectedLayananId = value;
                            layananIdController.text = value?.toString() ?? '';
                          });
                        },
                        items: layananList.map((Map<String, dynamic> layanan) {
                          return DropdownMenuItem<int>(
                            value: layanan['id'] as int,
                            child: Text(layanan['namaLayanan'] as String),
                          );
                        }).toList(),
                      ),
                    ),
                    Visibility(
                      visible: selectedLayananId == 2 || selectedLayananId == 3,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                        child: Text("Pilih Jenis Kamar", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      ),
                    ),
                    Visibility(
                      visible: selectedLayananId == 2 || selectedLayananId == 3,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10.0),
                        child: DropdownButtonFormField<int>(
                          decoration: InputDecoration(
                            hintText: 'Pilih Jenis Kamar',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12.0)),
                              borderSide: BorderSide(color: Colors.grey, width: 2),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10.0)),
                              borderSide: BorderSide(color: Colors.green, width: 2),
                            ),
                          ),
                          value: selectedKamarId,
                          onChanged: (int? value) {
                            setState(() {
                              selectedKamarId = value;
                              kamarIdController.text = value?.toString() ?? '';
                            });
                          },
                          items: kamarList.map((Map<String, dynamic> kamar) {
                            return DropdownMenuItem<int>(
                              value: kamar['id'] as int,
                              child: Text(kamar['jenisKamar'] as String),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Pilih Obat", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    for (var obat in selectedObatList)
                      ObatContainer(
                        obat: obat,
                        onDelete: () {
                          setState(() {
                            selectedObatList.remove(obat);
                          });
                        },
                        onEditQuantity: (int newQuantity) {
                          setState(() {
                            obat["kuantitas"] = newQuantity;
                          });
                        },
                        namaObat: getNamaObat(obat["obatId"])
                      ),
                    if (selectedObatId == null)
                      buildDropdownContainer()
                    else
                      buildSelectedObatContainer(),
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Diagnosis", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: diagnosisController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Diagnosis',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                        Container(
                          padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                          child: Text("Pesan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10.0),
                          child: TextField(
                            controller: pesanController..text,
                            enabled: true,
                            // readOnly: true,
                            autocorrect: true,
                            decoration: InputDecoration(
                              hintText: 'Pesan',
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                borderSide: BorderSide(color: Colors.grey, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.green, width: 2),
                              ),
                            ),),),

                  ],
                ),
              ),

                new GestureDetector(
                    onTap: (){
                      updateTreatment();
                    },
                    child:  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 20.0),
                      alignment: Alignment.topLeft,
                      child: Text("Save", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18.0),),
                    ),
                ),
                ],
              ),
            ));
  }

  Widget buildDropdownContainer() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: [
          DropdownButtonFormField<int>(
            decoration: InputDecoration(
              hintText: 'Pilih Obat',
              hintStyle: TextStyle(color: Colors.grey),
              filled: true,
              fillColor: Colors.white70,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.green, width: 2),
              ),
            ),
            value: selectedObatId,
            onChanged: (int? value) {
              setState(() {
                selectedObatId = value;
                obatIdController.text = value?.toString() ?? '';
              });
            },
            items: obatList.map((Map<String, dynamic> obat) {
              return DropdownMenuItem<int>(
                value: obat['id'] as int,
                child: Text(obat['namaObat'] as String),
              );
            }).toList(),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget buildSelectedObatContainer() {
    return ObatContainer(
      obat: {
        "obatId": selectedObatId,
        "kuantitas": int.tryParse(kuantitasObatController.text) ?? 1,
        "namaObat": getNamaObat(selectedObatId),
      },
      onDelete: () {
        setState(() {
          selectedObatId = null;
          obatIdController.text = '';
          kuantitasObatController.text = '';
        });
      },
      onEditQuantity: (int newQuantity) {
        setState(() {
          selectedObatList.add({
            "obatId": selectedObatId,
            "kuantitas": newQuantity,
          });
          selectedObatId = null;
          obatIdController.text = '';
          kuantitasObatController.text = '';
        });
      },
      namaObat: getNamaObat(selectedObatId)
    );
  }
}
