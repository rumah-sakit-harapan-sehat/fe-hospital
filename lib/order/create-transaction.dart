import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/dashboard/dashboard-resepsionis.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class CreateTransaction extends StatefulWidget {
  @override
  _CreateTransactionState createState() => new _CreateTransactionState();
}

class _CreateTransactionState extends State<CreateTransaction> {
  late SharedPreferences prefs;
  late List data = [];
  late List dataLayanan = [];
  List<Map<String, dynamic>> dokterList = [];
  int? selectedDokterId;
  List<Map<String, dynamic>> layananList = [];
  int? selecteLayananId;
  DateTime? selectedDate;

  String namaLayanan = "";

  TextEditingController biayaAdminController = new TextEditingController();
  TextEditingController totalHariRawatInapController = new TextEditingController();
  TextEditingController treatmentIdController = new TextEditingController();
  TextEditingController biayaOperasiController = new TextEditingController();

  Future<dynamic> fetchTreatment() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/treatment/' + treatmentIdController.text;
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      final Map<String, dynamic> data = jsonDecode(response.body);
      namaLayanan = data['data']['layanan']['namaLayanan'];
    });
    return data;
  }

  Future<void> fetchDataOnInitialize() async {
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  void createMedicalRecord() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    var response = await http.post(Uri.parse(ApiService.urlApi +'/transaksi/add'),
      headers: <String, String>{
        'token': prefs.getString("token").toString(),
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $bearerToken',
      },
      body: jsonEncode(<String, String>{
        'resepsionisId': prefs.getInt("userId").toString(),
        'biayaAdmin': biayaAdminController.text,
        'totalHariRawatInap': totalHariRawatInapController.text,
        'treatmentId': treatmentIdController.text,
        'biayaOperasi': biayaOperasiController.text,
      }),);
    setState(() {
      var data = json.decode(response.body);

      if(response.statusCode == 201){
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new DashboardResepsionis(),
        ));
      } else {
        Fluttertoast.showToast(
            msg: "Transaction Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.blue,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                    alignment: Alignment.topLeft,
                    child: Text('Transaksi', style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 22.0),),
                  ),
                ],
              ),

          Container(
            margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                  child: Text("ID Treatment", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: treatmentIdController..text,
                    enabled: true,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'ID Treatment',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Colors.green, width: 2),
                      ),
                    ),),),
                    new GestureDetector(
                      onTap: (){
                        fetchTreatment();
                      },
                      child:  Container(
                        margin: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 20.0),
                        alignment: Alignment.topLeft,
                        child: Text("Input", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18.0),),
                      ),
                  ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Biaya Admin", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: biayaAdminController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Biaya Admin',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                    Visibility(
                      visible: namaLayanan == "Rawat Inap" || namaLayanan == "Operasi",
                      child: Container(
                        padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                        child: Text("Total Hari Rawat Inap", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    ),
                    Visibility(
                      visible: namaLayanan == "Rawat Inap" || namaLayanan == "Operasi",
                      child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: totalHariRawatInapController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Total Hari Rawat Inap',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                    ),
                    Visibility(
                      visible: namaLayanan == "Operasi",
                      child: Container(
                        padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                        child: Text("Biaya Operasi", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    ),
                    Visibility(
                      visible: namaLayanan == "Operasi",
                      child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: biayaOperasiController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Biaya Operasi',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                    ),

              ],
            ),
          ),

            new GestureDetector(
                onTap: (){
                  createMedicalRecord();
                },
                child:  Container(
                  margin: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Text("Save", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18.0),),
                ),
            ),
            ],
          ),
        ));
  }
}
