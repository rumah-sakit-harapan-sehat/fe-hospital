import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class QRCodeImageTransaction extends StatefulWidget {
  @override
  _QRCodeImageTransactionState createState() => _QRCodeImageTransactionState();
}

class _QRCodeImageTransactionState extends State<QRCodeImageTransaction> {
  late SharedPreferences prefs;
  Uint8List? imageBytes;
  Uint8List? pdfBytes;

  Future<void> loadImage() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/transaksi/' + prefs.getInt("transaksiId").toString() + '/generate-qrcode';
    final response = await http.get(Uri.parse(url), headers: {
      'Accept': 'image/png',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });

    if (response.statusCode == 200) {
      setState(() {
        imageBytes = response.bodyBytes;
      });
    } else {
      print('Failed to load image: ${response.statusCode}');
    }
  }

  Future<void> fetchDataOnInitialize() async {
    await loadImage();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Code Image'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: imageBytes != null
                ? Image.memory(imageBytes!)
                : CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
