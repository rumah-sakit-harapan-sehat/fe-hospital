import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../api/api-service.dart';

class PdfViewContainer extends StatefulWidget {
  @override
  _PdfViewContainerState createState() => _PdfViewContainerState();
}

class _PdfViewContainerState extends State<PdfViewContainer> {
  late SharedPreferences prefs;
  late Uint8List pdfBytes = Uint8List(0);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('PDF Viewer'),
        ),
        body: Column(
          children: [
            ElevatedButton(
              onPressed: () {
                // Replace the URL with your API endpoint
                fetchPdfData();
              },
              child: Text('Fetch PDF'),
            ),
            Expanded(
              child: pdfBytes.isNotEmpty
                ? SfPdfViewer.memory(pdfBytes)
                : Center(child: CircularProgressIndicator()),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> fetchPdfData() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/transaksi/export-to-pdf/1';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Accept': 'application/pdf',
        'Authorization': 'Bearer $bearerToken',
      });

      if (response.statusCode == 200) {
        final fetchedPdfBytes = response.bodyBytes;
        setState(() {
          pdfBytes = fetchedPdfBytes;
        });
      } else {
        // Handle API error
        print('Failed to fetch PDF data: ${response.statusCode}');
      }
    } catch (error) {
      // Handle network error
      print('Error fetching PDF data: $error');
    }
  }

  Future<void> fetchDataOnInitialize() async {
    await fetchPdfData();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }
}
