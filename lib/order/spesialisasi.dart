import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/order/create-visit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class Spesialiasi extends StatefulWidget {
  @override
  _SpesialiasiState createState() => new _SpesialiasiState();
}

class _SpesialiasiState extends State<Spesialiasi> {
  late SharedPreferences prefs;
  late List data = [];

  Future<List<dynamic>> makeRequest() async {
    prefs = await SharedPreferences.getInstance();
    String url = ApiService.urlApi + '/spesialis';

    try {
      var response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
      });

      if (response.statusCode == 200) {
        var extractData = json.decode(response.body);
        setState(() {
          data = List<Map<String, dynamic>>.from(extractData['data']);
        });

        Fluttertoast.showToast(
          msg: "Data fetched successfully",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 16.0,
        );

        return data;
      } else {
        throw Exception('Failed to load data: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Failed to load data: $error');
    }
  }

  @override
  void initState() {
    this.makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: Colors.blue,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                alignment: Alignment.topLeft,
                child: Text(
                  "Daftar Spesialis",
                  style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, i) {
                  return Card(

                    child: Column(children: [
                      ListTile(
                      title: Text(
                        data[i]["namaSpesialis"],
                        style: TextStyle(color: Colors.black),
                      ),
                      leading: Image.network(
                        data[i]['urlImage'],
                        fit: BoxFit.cover,
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.topCenter,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.arrow_forward_sharp),
                        onPressed: () {
                          prefs.setString("spesialisasi", data[i]["namaSpesialis"]);
                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new CreateVisit(),
                          ));
                        },

                      ),
                    ),
                    ],)
                    
                  );
                }),
          ),
        ],
      ),
    ));
  }
}
