import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:jdt13/dashboard/history-pasien.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../api/api-service.dart';

class CreateVisit extends StatefulWidget {
  @override
  _CreateVisitState createState() => new _CreateVisitState();
}

class _CreateVisitState extends State<CreateVisit> {
  late List data = [];
  late List dataLayanan = [];
  List<Map<String, dynamic>> dokterList = [];
  int? selectedDokterId;
  List<Map<String, dynamic>> layananList = [];
  int? selecteLayananId;
  DateTime? selectedDate;

  TextEditingController dokterIdController = new TextEditingController();
  TextEditingController layananIdController = new TextEditingController();
  TextEditingController tanggalKonsultasiController = new TextEditingController();

  Future<List<Map<String, dynamic>>> fetchDokter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/dokter/spesialisasi/' + prefs.getString("spesialisasi").toString();

    var response = await http.get(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Bearer $bearerToken',
      },
    );

    if (response.statusCode == 200) {
      var extractData = json.decode(response.body);

      if (extractData['data'] is List) {
        setState(() {
          dokterList = List<Map<String, dynamic>>.from(extractData['data']);
          print(dokterList);
        });
      } else if (extractData['data'] is Map) {
        setState(() {
          dokterList = [Map<String, dynamic>.from(extractData['data'])];
        });
      } else {
        throw Exception('Unexpected format of data');
      }

      Fluttertoast.showToast(
        msg: "Data fetched successfully",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        fontSize: 16.0,
      );

      return dokterList;
    } else {
      throw Exception('Failed to load dokter data');
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    await initializeDateFormatting('id_ID');

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate ?? DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2050),
    );

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        tanggalKonsultasiController.text = DateFormat('yyyy-MM-dd', 'id_ID').format(picked);
      });
    }
  }

  Future<dynamic> fetchLayananData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/layanan';
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      final List<dynamic> data = jsonDecode(response.body);
      layananList = List<Map<String, dynamic>>.from(data);
    });
    return dataLayanan;
  }

  Future<void> fetchDataOnInitialize() async {
    await fetchDokter();
    await fetchLayananData();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  void updateOrder() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    var response = await http.post(Uri.parse(ApiService.urlApi +'/treatment/add'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $bearerToken',
      },
      body: jsonEncode(<String, String>{
        'pasienId': prefs.getInt("userId").toString(),
        'dokterId': dokterIdController.text,
        'tanggalKonsul': tanggalKonsultasiController.text
      }),);
    setState(() {
      var data = json.decode(response.body);

      if(response.statusCode == 201){
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new HistoryPasien(),
        ));
      } else {
        Fluttertoast.showToast(
            msg: "Booking Consultation Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.blue,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                    alignment: Alignment.topLeft,
                    child: Text("Buat Janji", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 22.0),),
                  ),
                ],
              ),

          Container(
            margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                  child: Text("Pilih Dokter", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10.0),
                  child: DropdownButtonFormField<int>(
                    decoration: InputDecoration(
                      hintText: 'Pilih Dokter',
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Colors.green, width: 2),
                      ),
                    ),
                    value: selectedDokterId,
                    onChanged: (int? value) {
                      setState(() {
                        selectedDokterId = value;
                        dokterIdController.text = value?.toString() ?? '';
                      });
                    },
                    items: dokterList.map((Map<String, dynamic> spesialis) {
                      return DropdownMenuItem<int>(
                        value: spesialis['id'] as int,
                        child: Text(spesialis['namaDokter'] as String),
                      );
                    }).toList(),
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                    child: Text(
                      "Tanggal Konsultasi",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _selectDate(context),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: tanggalKonsultasiController,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: selectedDate != null
                              ? DateFormat.yMMMMd('id_ID').format(selectedDate!)
                              : 'Masukkan Tanggal Konsultasi',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),

            new GestureDetector(
                onTap: (){
                  updateOrder();
                },
                child:  Container(
                  margin: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Text("Buat Janji", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18.0),),
                ),
            ),
            ],
          ),
        ));
  }
}
