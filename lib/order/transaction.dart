import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/order/QRCodeImageTransaction.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../api/api-service.dart';

class Transaction extends StatefulWidget {
  @override
  _TransactionState createState() => new _TransactionState();
}

class _TransactionState extends State<Transaction> {
  late SharedPreferences prefs;
  late List data = [];

  Future<List<dynamic>> makeRequest() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/transaksi/pasien/' + prefs.getInt("userId").toString();
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      var extractData = json.decode(response.body);
      data = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return data;
  }

  String formatDate(String date) {
    initializeDateFormatting('id_ID');
    final parsedDate = DateFormat('dd/MM/yyyy').parse(date);
    final formattedDate = DateFormat.yMMMMd('id_ID').format(parsedDate);
    return formattedDate;
  }

  @override
  void initState() {
    this.makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: Colors.blue,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                alignment: Alignment.topLeft,
                child: Text(
                  "Transaksi",
                  style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, i) {
                  return Card(

                    child: Column(children: [
                      ListTile(
                      title: Text(
                        formatDate(data[i]["tanggalTransaksi"]),
                        style: TextStyle(color: Colors.black),
                      ),
                      subtitle: Text(
                        'ID Treatment: ' + data[i]["treatmentId"].toString() +
                        '\nBiaya Obat: ' + data[i]["biayaObat"].toString() +
                        '\nBiaya Dokter:' +
                        data[i]["biayaDokter"].toString() +
                        '\nBiaya Layanan: ' +
                        data[i]["biayaLayanan"].toString() +
                        '\nBiaya Admin: ' +
                        data[i]["biayaAdmin"].toString() +
                        '\nTotal: ' +
                        (data[i]["biayaObat"] + data[i]["biayaDokter"] + data[i]["biayaLayanan"] + data[i]["biayaAdmin"]).toString(), 
                        style: TextStyle(color: Colors.blue[450]),
                      ),
                      leading: Image.network(
                        "https://cdn-icons-png.flaticon.com/512/3186/3186949.png",
                        fit: BoxFit.cover,
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.topCenter,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.arrow_forward_sharp),
                        onPressed: () {
                          prefs.setInt("transaksiId", data[i]["id"]);
                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context) =>
                                new QRCodeImageTransaction(),
                          ));
                        },

                      ),
                    ),
                    ],)
                    
                  );
                }),
          ),
        ],
      ),
    ));
  }
}
