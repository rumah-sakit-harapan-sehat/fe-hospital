import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/order/update-treatment.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class CreateMedicalRecord extends StatefulWidget {
  @override
  _CreateMedicalRecordState createState() => new _CreateMedicalRecordState();
}

class _CreateMedicalRecordState extends State<CreateMedicalRecord> {
  late SharedPreferences prefs;
  late List data = [];
  late List dataLayanan = [];
  List<Map<String, dynamic>> dokterList = [];
  int? selectedDokterId;
  List<Map<String, dynamic>> layananList = [];
  int? selecteLayananId;
  DateTime? selectedDate;

  TextEditingController tinggiBadanController = new TextEditingController();
  TextEditingController beratBadanController = new TextEditingController();
  TextEditingController tensiDarahController = new TextEditingController();
  TextEditingController gejalaController = new TextEditingController();
  TextEditingController alergiController = new TextEditingController();

  Future<dynamic> fetchDokter() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/treatment?id='+prefs.getInt("treatmentId").toString();
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      final List<dynamic> data = jsonDecode(response.body);
      dokterList = List<Map<String, dynamic>>.from(data);
    });
    return data;
  }

  Future<void> fetchDataOnInitialize() async {
    await fetchDokter();
  }

  @override
  void initState() {
    super.initState();
    this.fetchDataOnInitialize();
  }

  void createMedicalRecord() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    var response = await http.post(Uri.parse(ApiService.urlApi +'/rekamMedis/add'),
      headers: <String, String>{
        'token': prefs.getString("token").toString(),
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $bearerToken',
      },
      body: jsonEncode(<String, String>{
        'pasienId': prefs.getInt("pasienId").toString(),
        'treatmentId': prefs.getInt("treatmentId").toString(),
        'tinggiBadan': tinggiBadanController.text,
        'beratBadan': beratBadanController.text,
        'tensiDarah': tensiDarahController.text,
        'alergi': alergiController.text,
        'gejala': gejalaController.text
      }),);
    setState(() {
      var data = json.decode(response.body);

      if(response.statusCode == 201){
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new UpdateTreatment(),
        ));
      } else {
        Fluttertoast.showToast(
            msg: "Save Medical Record Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [

                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                      alignment: Alignment.topLeft,
                      child: Icon(
                        Icons.arrow_back_ios_sharp,
                        color: Colors.blue,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                    alignment: Alignment.topLeft,
                    child: Text('Rekam Medis', style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 22.0),),
                  ),
                ],
              ),

          Container(
            margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                  child: Text("Tensi Darah", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: tensiDarahController..text,
                    enabled: true,
                    autocorrect: true,
                    decoration: InputDecoration(
                      hintText: 'Tensi Darah',
                      //prefixIcon: Icon(Icons.email),
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12.0)),
                        borderSide: BorderSide(color: Colors.grey, width: 2),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide: BorderSide(color: Colors.green, width: 2),
                      ),
                    ),),),
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Tinggi Badan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: tinggiBadanController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Tinggi Badan',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                    Container(
                      padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                      child: Text("Berat Badan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: beratBadanController..text,
                        enabled: true,
                        autocorrect: true,
                        decoration: InputDecoration(
                          hintText: 'Berat Badan',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),),),
                      Container(
                        padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                        child: Text("Gejala", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10.0),
                        child: TextField(
                          controller: gejalaController..text,
                          enabled: true,
                          autocorrect: true,
                          decoration: InputDecoration(
                            hintText: 'Gejala',
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12.0)),
                              borderSide: BorderSide(color: Colors.grey, width: 2),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(10.0)),
                              borderSide: BorderSide(color: Colors.green, width: 2),
                            ),
                          ),),),
                          Container(
                            padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                            child: Text("Alergi", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(10.0),
                            child: TextField(
                              controller: alergiController..text,
                              enabled: true,
                              autocorrect: true,
                              decoration: InputDecoration(
                                hintText: 'Alergi',
                                hintStyle: TextStyle(color: Colors.grey),
                                filled: true,
                                fillColor: Colors.white70,
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12.0)),
                                  borderSide: BorderSide(color: Colors.grey, width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  borderSide: BorderSide(color: Colors.green, width: 2),
                                ),
                              ),),),

              ],
            ),
          ),

            new GestureDetector(
                onTap: (){
                  createMedicalRecord();
                },
                child:  Container(
                  margin: EdgeInsets.fromLTRB(20.0, 10.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Text("Save", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 18.0),),
                ),
            ),
            ],
          ),
        ));
  }
}
