import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import '../api/api-service.dart';

class MedicalRecord extends StatefulWidget {
  @override
  _MedicalRecordState createState() => new _MedicalRecordState();
}

class _MedicalRecordState extends State<MedicalRecord> {
  late SharedPreferences prefs;
  late List data = [];

  Future<List<dynamic>> makeRequest() async {
    prefs = await SharedPreferences.getInstance();
    String bearerToken = prefs.getString("token").toString();
    String url = ApiService.urlApi + '/rekamMedis/pasien/' + prefs.getInt("userId").toString();
    var response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $bearerToken',
    });
    setState(() {
      var extractData = json.decode(response.body);
      data = List<Map<String, dynamic>>.from(extractData['data']);
    });
    return data;
  }

  String formatDate(String date) {
    initializeDateFormatting('id_ID');
    final parsedDate = DateTime.parse(date);
    final formattedDate = DateFormat.yMMMMd('id_ID').format(parsedDate);
    return formattedDate;
  }

  @override
  void initState() {
    this.makeRequest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Icon(
                    Icons.arrow_back_ios_sharp,
                    color: Colors.blue,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                alignment: Alignment.topLeft,
                child: Text(
                  "Rekam Medis",
                  style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, i) {
                  return Card(

                    child: Column(children: [
                      ListTile(
                      title: Text(
                        formatDate(data[i]["tanggalTreatment"]),
                        style: TextStyle(color: Colors.black),
                      ),
                      subtitle: Text(
                        'Tinggi Badan: ' + data[i]["tinggiBadan"].toString() +
                        '\nBerat Badan:' +
                        data[i]["beratBadan"].toString() +
                        '\nTensi Darah: ' +
                        data[i]["tensiDarah"].toString() +
                        '\nGejala: ' +
                        data[i]["gejala"].toString() +
                        '\nAlergi: ' +
                        data[i]["alergi"].toString(), 
                        style: TextStyle(color: Colors.blue[450]),
                      ),
                      leading: Image.network(
                        "https://cdn-icons-png.flaticon.com/512/7031/7031039.png",//disini link urlnya untuk imagenya
                        fit: BoxFit.cover,
                        height: 40.0,
                        width: 40.0,
                        alignment: Alignment.topCenter,
                      ),
                    ),
                    ],)
                    
                  );
                }),
          ),
        ],
      ),
    ));
  }
}
