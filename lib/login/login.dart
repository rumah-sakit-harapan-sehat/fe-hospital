import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jdt13/dashboard/dashboard-dokter.dart';
import 'package:jdt13/dashboard/dashboard-resepsionis.dart';
import 'package:jdt13/dashboard/dashboard-pasien.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/registration/registration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/api-service.dart';

class Login extends StatefulWidget {

  @override
  _LoginState createState() => new _LoginState();

}

class _LoginState extends State<Login> {

  TextEditingController usernameController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  void signin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var response = await http.post(Uri.parse(ApiService.urlApi +'/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'username': usernameController.text,
        'password': passwordController.text
      }),
    );

    print(response.statusCode);

    setState(() {
      var data = json.decode(response.body);
      final userId = data['data']['userId'];
      final token = data['data']['token'];
      final role = data['data']['role'];

      if(response.statusCode == 401){
        Fluttertoast.showToast(
            msg: "Invalid Email & Password",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      } else if(response.statusCode == 404){
        Fluttertoast.showToast(
            msg: "User Does Not Exist",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0
        );
      } else if(role == "DOKTER"){
        prefs.setInt("userId", userId);
        prefs.setString("token", token);
        prefs.setString("role", role);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new DashboardDokter(),
        ));
      } else if(role == "RESEPSIONIS"){
        prefs.setInt("userId", userId);
        prefs.setString("token", token);
        prefs.setString("role", role);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new DashboardResepsionis(),
        ));
      } else {
        prefs.setInt("userId", userId);
        prefs.setString("token", token);
        prefs.setString("role", role);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new DashboardPasien(),
        ));
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _header(context),
                _textFieldPhone(context),
                _textFieldPassword(context),
                _buttonSend(context),
                _registration(context)
              ],
            )),
      ),
    );
  }

  _header(BuildContext context) {
    return
      Center(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 140, 0, 0),
          height: 150.0,
          width: 170.0,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/restaurant.png'),
              fit: BoxFit.fill,
            ),
          ),
        ),
      );
  }

  _textFieldPhone(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 50, 20, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Username',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 8),
          TextField(
            controller: usernameController..text,
            autocorrect: true,
            decoration: InputDecoration(
              hintText: 'Masukan Username',
              hintStyle: TextStyle(color: Colors.grey),
              filled: true,
              fillColor: Colors.white70,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.blue, width: 2),
              ),
            ),
          ),
        ],
      ),
    );

  }

  _textFieldPassword(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Password',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 8),
          TextField(
            controller: passwordController,
            obscureText: true,
            autocorrect: true,
            decoration: InputDecoration(
              hintText: 'Masukan Password',
              hintStyle: TextStyle(color: Colors.grey),
              filled: true,
              fillColor: Colors.white70,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0)),
                borderSide: BorderSide(color: Colors.grey, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.blue, width: 2),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _buttonSend(BuildContext context) {
    return Container(
      height: 45.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: Color(0xFF009FD8),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
        ),
        onPressed: () {
          signin();
        },
        child: Text(
          "Masuk",
          style: TextStyle(
            color: Color(0xffffffff),
          ),
        ),
      ),
    );

  }

  _registration(BuildContext context) {
    return Container(
      height: 45.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
      child: Ink(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Color(0xFF009FD8), width: 2.0),
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => Registration(),
              ),
            );
          },
          child: Center(
            child: Text(
              "Daftar",
              style: TextStyle(
                color: Color(0xFF009FD8),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );

  }

}
