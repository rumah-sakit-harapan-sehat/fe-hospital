import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:jdt13/login/login.dart';
import '../api/api-service.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => new _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  TextEditingController nikController = new TextEditingController();
  TextEditingController nipController = new TextEditingController();
  TextEditingController namaController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController alamatController = new TextEditingController();
  TextEditingController tanggalLahirController = new TextEditingController();
  TextEditingController jenisKelaminController = new TextEditingController();
  TextEditingController spesialisIdController = new TextEditingController();

  bool useFirstPage = true;

  String selectedJenisKelamin = '';

  DateTime? selectedDate;

  List<Map<String, dynamic>> spesialisList = []; 
  int? selectedSpesialisId;

  _RegistrationState() {
    initializeDateFormatting('id_ID');
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate ?? DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );

    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
        tanggalLahirController.text = DateFormat.yMMMMd('id_ID').format(picked);
      });
    }
  }

  Future<void> fetchSpesialisData() async {
    final response = await http.get(Uri.parse(ApiService.urlApi + '/spesialis'));
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = jsonDecode(response.body);
      final List<dynamic> data = responseData['data'];
      setState(() {
        spesialisList = List<Map<String, dynamic>>.from(data);
      });
    } else {
      throw Exception('Failed to load spesialis data');
    }
  }

  @override
  void initState() {
    super.initState();
    fetchSpesialisData(); // Fetch spesialis data when the widget is initialized
  }

  void registration() async {
    var response = await http.post(
      Uri.parse(ApiService.urlApi + '/register/dokter/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'nip': nipController.text,
        'namaDokter': namaController.text,
        'password': passwordController.text,
        'alamat': alamatController.text,
        'tanggalLahir': tanggalLahirController.text,
        'jenisKelamin': selectedJenisKelamin,
        'spesialisId': selectedSpesialisId.toString()
      }),
    );
    setState(() {
      final parsedJson = jsonDecode(response.body);
      if (response.statusCode == 201) {
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new Login(),
        ));
      } else if(response.statusCode == 400){
        Fluttertoast.showToast(
            msg: parsedJson['errors'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: "Registered Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }

  void registrationPatient() async {
    var response = await http.post(
      Uri.parse(ApiService.urlApi + '/register/pasien/'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'nik': nikController.text,
        'namaPasien': namaController.text,
        'password': passwordController.text,
        'alamat': alamatController.text,
        'tanggalLahir': tanggalLahirController.text,
        'jenisKelamin': selectedJenisKelamin,
      }),
    );
    setState(() {
      final parsedJson = jsonDecode(response.body);
      if (response.statusCode == 201) {
        Fluttertoast.showToast(
            msg: "Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
        Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new Login(),
        ));
      } else if(response.statusCode == 400){
        Fluttertoast.showToast(
            msg: parsedJson['errors'].toString(),
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        Fluttertoast.showToast(
            msg: "Registered Not Success",
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                    alignment: Alignment.topLeft,
                    child: Icon(
                      Icons.arrow_back_ios_sharp,
                      color: Colors.blue,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10.0, 30.0, 0.0, 20.0),
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Registrasi",
                    style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text("Dokter"),
                      CupertinoSwitch(
                        value: !useFirstPage,
                        onChanged: (value) {
                          setState(() {
                            useFirstPage = !value;
                          });
                        },
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text("Pasien"),
                      CupertinoSwitch(
                        value: useFirstPage,
                        onChanged: (value) {
                          setState(() {
                            useFirstPage = value;
                          });
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),

              useFirstPage ? _buildFirstRegistrationPage() : _buildSecondRegistrationPage(),
          ],
        ),
      ),
    ));
  }

  Widget _buildFirstRegistrationPage() {
    return Column(
      children: [
        
          Container(
              margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "NIK",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: nikController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan NIK',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Nama",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: namaController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Nama',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Password",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: passwordController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Password',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Alamat",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: alamatController..text,
                      enabled: true,
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Alamat',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                    child: Text(
                      "Tanggal Lahir",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _selectDate(context),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: tanggalLahirController,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: selectedDate != null
                              ? DateFormat.yMMMMd('id_ID').format(selectedDate!)
                              : 'Masukkan Tanggal Lahir',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Jenis Kelamin",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),

              Row(
                children: [
                  Radio(
                    value: 'Laki-Laki',
                    groupValue: selectedJenisKelamin,
                    onChanged: (value) {
                      setState(() {
                        selectedJenisKelamin = value.toString();
                      });
                    },
                  ),
                  Text('Laki-Laki'),

                  Radio(
                    value: 'Perempuan',
                    groupValue: selectedJenisKelamin,
                    onChanged: (value) {
                      setState(() {
                        selectedJenisKelamin = value.toString();
                      });
                    },
                  ),
                  Text('Perempuan'),
                ],
              ),
                ],
              ),
            ),

        Container(
          height: 45.0,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(10.0),
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            onPressed: () {
              if (useFirstPage) {
                registrationPatient();
              } else {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Login()),
                );
              }
            },
            child: Text(
              "Create an Account",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSecondRegistrationPage() {
    return Column(
      children: [
          Container(
              margin: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 0.0),
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "NIP",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: nipController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan NIP',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Nama",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: namaController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Nama',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Password",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: passwordController..text,
                      autocorrect: true,
                      enabled: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Password',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Alamat",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: alamatController..text,
                      enabled: true,
                      autocorrect: true,
                      decoration: InputDecoration(
                        hintText: 'Masukan Alamat',
                        //prefixIcon: Icon(Icons.email),
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 2),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.green, width: 2),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 0.0),
                    child: Text(
                      "Tanggal Lahir",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => _selectDate(context),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: tanggalLahirController,
                        enabled: false,
                        decoration: InputDecoration(
                          hintText: selectedDate != null
                              ? DateFormat.yMMMMd('id_ID').format(selectedDate!)
                              : 'Masukkan Tanggal Lahir',
                          hintStyle: TextStyle(color: Colors.grey),
                          filled: true,
                          fillColor: Colors.white70,
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12.0)),
                            borderSide: BorderSide(color: Colors.grey, width: 2),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(10.0)),
                            borderSide: BorderSide(color: Colors.green, width: 2),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
                    child: Text(
                      "Jenis Kelamin",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),

              Row(
                children: [
                  Radio(
                    value: 'Laki-Laki',
                    groupValue: selectedJenisKelamin,
                    onChanged: (value) {
                      setState(() {
                        selectedJenisKelamin = value.toString();
                      });
                    },
                  ),
                  Text('Laki-Laki'),

                  Radio(
                    value: 'Perempuan',
                    groupValue: selectedJenisKelamin,
                    onChanged: (value) {
                      setState(() {
                        selectedJenisKelamin = value.toString();
                      });
                    },
                  ),
                  Text('Perempuan'),
                ],
              ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(20.0, 00.0, 0.0, 0.0),
              child: Text(
                "Spesialisasi",
                style:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10.0),
                child: DropdownButtonFormField<int>(
                  decoration: InputDecoration(
                    hintText: 'Pilih Spesialisasi',
                    hintStyle: TextStyle(color: Colors.grey),
                    filled: true,
                    fillColor: Colors.white70,
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                      borderSide: BorderSide(color: Colors.grey, width: 2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.green, width: 2),
                    ),
                  ),
                  value: selectedSpesialisId,
                  onChanged: (int? value) {
                    setState(() {
                      selectedSpesialisId = value;
                      spesialisIdController.text = value?.toString() ?? '';
                    });
                  },
                  items: spesialisList.map((Map<String, dynamic> spesialis) {
                    return DropdownMenuItem<int>(
                      value: spesialis['id'] as int,
                      child: Text(spesialis['namaSpesialis'] as String),
                    );
                  }).toList(),
                ),
              ),

        Container(
          height: 45.0,
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.all(10.0),
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Colors.blue,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            onPressed: () {
              if (useFirstPage) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Registration()),
                );
              } else {
                registration();
              }
            },
            child: Text(
              "Create an Account",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
